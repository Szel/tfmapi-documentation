using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

class Parser
{
    private const string FunctionPattern =
        "•\\s+" + // begining
        "([^\\s]+)" + // function name
        "\\s+\\((.*)\\)"; // args

    private const string ArgumentPattern =
        "-\\s+" +
        "([^\\s]+)" + // argument name
        "\\s+\\((.+)\\)" + // type
        "\\s+:\\s+(.+)"; // description

    public IEnumerable<Function> Parse(string fileContent)
    {
        var reader = new StringReader(fileContent);
        var allFunctions = new List<Function>();
        Function currentFunction = null;
        Argument currentArgument = null;
        var argumentsStarted = false;
        while (true)
        {
            var line = reader.ReadLine();
            if (line == null)
                break;  // end of file
            line = line.Trim();

            if (string.IsNullOrWhiteSpace(line))
                continue;

            var functionMatch = Regex.Match(line, FunctionPattern);
            if (functionMatch.Success)
            {
                var arguments = functionMatch.Groups[2].Value
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Where(a => !string.IsNullOrWhiteSpace(a))
                    .Select(a => new Argument()
                    {
                        Name = a.Trim()
                    });
                currentFunction = new Function()
                {
                    Name = functionMatch.Groups[1].Value,
                    Arguments = arguments.ToList()
                };

                allFunctions.Add(currentFunction);
                argumentsStarted=false;
            }
            else if (currentFunction != null && argumentsStarted)
            {
                var argumentMatch = Regex.Match(line, ArgumentPattern);
                if(argumentMatch.Success){
                    currentArgument = currentFunction.Arguments
                        .Single(a=>a.Name == argumentMatch.Groups[1].Value);

                    currentArgument.Type = argumentMatch.Groups[2].Value;
                    currentArgument.Description = argumentMatch.Groups[3].Value;
                }else if(currentArgument != null)
                {
                    currentArgument.Description += Environment.NewLine + line;
                }
            }
            else if (currentFunction != null && (line == "Arguments:" || line == "Parameters:"))
            {
                argumentsStarted = true;
            }
            else if(currentFunction != null)
            {
                currentFunction.Description += line + Environment.NewLine;
            }
        }

        return allFunctions;
    }
}