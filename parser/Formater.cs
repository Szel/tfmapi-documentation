using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

class Formater
{
    private string template = 
@"<!DOCTYPE html>
<html lang=""en"">
<head>
    <meta charset=""UTF-8"">
    <link href=""style.css"" rel=""stylesheet"">
</head>
<body>
    <h1 class=""name"">{0}({1})</h1>
    <div class=""description"">{2}</div>
    <div class=""arguments"">
        {3}
    </div>
</body>
</html>";
    public void FormatEvents(IEnumerable<Function> events, string directory)
    {

        foreach (var ev in events)
        {
            var argBuilder = new StringBuilder();
            foreach (var arg in ev.Arguments)
            {
                argBuilder.AppendLine(string.Format(
                    "- <span class=\"arg-name\">{0}</span> ({1}) : {2}<br>",
                    arg.Name,
                    arg.Type,
                    arg.Description));
            }

            var html = string.Format(
                template, 
                ev.Name,
                string.Join(", ", ev.Arguments.Select(a=>a.Name)),
                ev.Description,
                argBuilder.ToString());

            File.WriteAllText(Path.Combine(directory, ev.FileName), html);
        }
    }

}