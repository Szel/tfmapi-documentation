﻿using System;
using System.IO;

namespace ConsoleApplication
{
    public class Program
    {
        private static Parser parser;
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            if (Directory.Exists("output"))
                Directory.Delete("output", true);

            Directory.CreateDirectory("output");
            parser = new Parser();

            var events = parser.Parse(File.ReadAllText("resources/events.txt"));
            var functions = parser.Parse(File.ReadAllText("resources/functions.txt"));

            var formater = new Formater();
            formater.FormatEvents(events, "output/");
            formater.FormatEvents(functions, "output/");

            var sqlgen = new SqlGenerator();
            File.WriteAllText("output/sqlscript.sql", sqlgen.GenerateScript(functions,events));
        }
    }
}
