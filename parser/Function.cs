using System.Collections.Generic;
using System.Text.RegularExpressions;

class Function
{
    public string Name { get; set; }
    public string FileName {get{
        return Regex.Replace(Name, "[^a-zA-Z]", "") + ".html";
    }}
    public string Description { get; set; }
    public IEnumerable<Argument> Arguments { get; set; }
}