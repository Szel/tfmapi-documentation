using System.Collections.Generic;
using System.Text;

class SqlGenerator
{
    public string GenerateScript(IEnumerable<Function> functions, IEnumerable<Function> events)
    {
        var insertFormat ="INSERT OR IGNORE INTO searchIndex(name, type, path) VALUES ('{0}', '{1}', '{2}');";
        var builder = new StringBuilder();

        foreach (var fun in functions)
        {
            builder.AppendLine(string.Format(insertFormat,
                fun.Name, "Function", fun.FileName));
        }

        foreach (var ev in events)
        {
            builder.AppendLine(string.Format(insertFormat,
                ev.Name, "Event", ev.FileName));
        }

        return builder.ToString();
    }
}